import { writeFile } from "fs/promises";

import { random } from "../src/random";
import { generateNodes, getArgsAsNames, loadNodes } from "./lib";

(async () => {
  const nodes = await loadNodes();
  const names = getArgsAsNames();

  const alreadyExist = names.filter(name => name in nodes);
  if(alreadyExist.length)
    throw new Error(`The following nodes already exist with the given name(s): ${alreadyExist.join(", ")}`);

  names.forEach(name => {
    nodes[name] = random().toString("base64");
  });

  const sortedNodes = Object.fromEntries(Object.entries(nodes).sort());
  const json = JSON.stringify(sortedNodes, null, 2);
  console.log(json);

  await writeFile('./src/id-base64.json', json + "\n");

  await generateNodes();
})();
