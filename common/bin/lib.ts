import { readFile, writeFile } from "fs/promises";

export const getArgsAsNames = () => {
  const names = process.argv.slice(2);

  if(!names.length)
    throw new Error('Name(s) required');

  return names.map(standardizeName);
};

export const generateNodes = async () => {
  const nodes = await loadNodes();

  const lines = Object.entries(nodes).map(([name, node]) => {
    return `export const ${name} = strToId("${node}");\n`;
  }).join('');

  const script = `import { strToId } from "./id";\n\n${lines}`;
  await writeFile('./src/ids.ts', script);
};

export const loadNodes = async () => {
  const nodes = await readFile('./src/id-base64.json', 'utf8');
  return JSON.parse(nodes);
};

export const standardizeName = (name: string) => {
  return name
    ?.replace(/ /g, '_',)
    .replace(/-/g, '_')
    .toUpperCase();
};
