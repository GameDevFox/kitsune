import { getArgsAsNames, loadNodes } from "./lib";

(async () => {
  const nodes = await loadNodes();

  const result: Record<string, string> = {};

  const names = getArgsAsNames();
  names.forEach(name => {
    if(!(name in nodes))
      return;

    const value = nodes[name];
    result[name] = encodeURIComponent(value);
  });

  console.log(result);
})();
