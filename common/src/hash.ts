import { createHash } from "crypto";

import { Id, bufferToId, sortIds } from "./id";
import { EDGE, LIST, NEXT, SET, STRING } from "./ids";
import { Kana, LSB } from "./kana";
import { Edge } from "./edge";

const algorithm = 'sha256';

const kana = Kana(LSB);

// See "hashString" below, maybe this is a good solution
export const hashStringRaw = (str: string) => {
  const hash = createHash(algorithm);
  hash.update(str);
  return hash.digest();
};

export const hash = (ids: Id[]) => {
  const hash = createHash(algorithm);

  ids.forEach(id => {
    const buffers = kana.writeValue(id.buffer);
    buffers.forEach(buffer => hash.update(buffer));
  });

  const buffer = hash.digest();
  return bufferToId(buffer);
};

export const hashNext = (node: Id) => hash([NEXT, node]);

export const hashEdge = ({ head, tail }: Edge) => hash([EDGE, head, tail]);

export type IdOrEdge = Id | [IdOrEdge, IdOrEdge];
export const hashEdges = (edges: [IdOrEdge, IdOrEdge]) => {
  let [headId, tailId] = edges;

  const head = Array.isArray(headId) ? hashEdges(headId) : headId;
  const tail = Array.isArray(tailId) ? hashEdges(tailId) : tailId;

  const result = hashEdge({ head, tail });
  return result;
};

// NOTE: There might be a clever way to collide specially
// constructed String hashes with other types of hashes
// by matching the string to match the same payload that
// is created in the "hash" function above.
// Therefore, all strings are hashed with a STRING prefix.
export const hashString = (str: string) => {
  const stringHash = bufferToId(hashStringRaw(str));
  return hash([STRING, stringHash]);
};

export const hashSet = (ids: Id[]) => {
  const unique: Record<string, Id> = {};
  ids.forEach(id => {
    unique[id.serial] = id;
  });

  const sortedIds = sortIds(Object.values(unique));
  return hash([SET, ...sortedIds]);
};

export const hashList = (ids: Id[]) => {
  return hash([LIST, ...ids]);
};
