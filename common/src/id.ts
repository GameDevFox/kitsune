import { readFileSync } from "fs";

export interface Id {
  serial: number,
  base64: string,
  buffer: Buffer,

  name?: string,
}

if((global as any)["kitsune_ids_loaded"])
  throw new Error("WARNING: Kitsune IDs are loaded more than once!");

(global as any)["kitsune_ids_loaded"] = true;

const nodes = readFileSync(__dirname + "/id-base64.json");
const names = Object.fromEntries(
  Object.entries(nodes).map(([key, value]) => [value, key])
);

let nodeCount = 0;

const ids: Record<string, Id> = {};

export const cacheId = (base64: string, buffer: Buffer) => {

  const id: Id = {
    serial: ++nodeCount,
    base64,
    buffer,
  };

  const name = lookupName(base64);
  if(name)
    id.name = name;

  ids[base64] = id;

  return id;
};

export const strToId = (base64: string) => {
  if(base64 in ids)
    return ids[base64];

  const buffer = Buffer.from(base64, 'base64');
  return cacheId(base64, buffer);
};

export const bufferToId = (buffer: Buffer) => {
  const base64 = buffer.toString('base64');

  if(base64 in ids)
    return ids[base64];

  return cacheId(base64, buffer);
};

export const isEqual = (a: Id, b: Id) => a.serial === b.serial;

export const compare = (a: Id, b: Id) => a.buffer.compare(b.buffer);

export const sortIds = (ids: Id[]) => [...ids].sort(compare);

export const lookupName = (base64: string) => names[base64];
