import { strToId, isEqual, lookupName } from "./id";
import { EDGE, HEAD } from "./ids";

describe('id()', () => {
  test('should assign the same serial for the same id', () => {
    const strA = 'q84UqCD7Ad7/ep8RLaaiz3uIZ/LYmktysqYjdn7rdww=';
    const strB = 'FbO/4NbaMSkgfk1RNiz4l8k2Ffgfh9JL7qhiX0ulxCY=';

    const ALPHA = strToId(strA);
    const BETA = strToId(strB);
    const DELTA = strToId(strA);


    expect(ALPHA === ALPHA).toBe(true);
    expect(ALPHA === BETA).toBe(false);
    expect(ALPHA === DELTA).toBe(true);

    expect(isEqual(ALPHA, ALPHA)).toBe(true);
    expect(isEqual(ALPHA, BETA)).toBe(false);
    expect(isEqual(ALPHA, DELTA)).toBe(true);
  });
});

describe("lookupName", () => {
  test("should be able to lookup name", () => {
    expect(lookupName(HEAD.base64)).toEqual('HEAD');
    expect(lookupName(EDGE.base64)).toEqual('EDGE');
  });
});
