import crypto from "crypto";
import { bufferToId } from "./id";

const DEFAULT_BIT_SIZE = 256;
const DEFAULT_BYTE_SIZE = DEFAULT_BIT_SIZE / 8;

export const random = (byteCount: number = DEFAULT_BYTE_SIZE) => crypto.randomBytes(byteCount);
export const randomId = () => bufferToId(random());
