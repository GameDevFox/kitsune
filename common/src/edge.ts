import { Id } from "./id";

export interface Edge {
  head: Id,
  tail: Id,
}
