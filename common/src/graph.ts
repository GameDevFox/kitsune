import { Edge } from "./edge";
import { Id } from "./id";

export interface GraphUpdate {
  add?: Edge[],
  remove?: Id[],
};

export interface GraphEnumerate {
  enumerate: (
    next: (edge: Edge) => void,
    done: () => void,
    error: (e: any) => void,
  ) => void,
};

export interface GraphReadable {
  read: (
    ids: Buffer[],
    next: (edge: Edge) => void,
    done: () => void,
    error: (e: any) => void,
  ) => void,
};

export interface GraphWriteable {
  write: (
    update: GraphUpdate,
    done: () => void,
    error: (e: any) => void,
  ) => void,
};

// export interface GraphListHeadTails {
//   listHeads: (
//     tail: Id,
//     next: (head: Id) => void,
//     error: (e: any) => void,
//   ) => void,

//   listTails: (
//     head: Id,
//     next: (tail: Id) => void,
//     error: (e: any) => void,
//   ) => void,
// };
