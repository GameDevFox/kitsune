
import { hash, hashEdge, hashList, hashNext, hashSet, hashString, hashStringRaw } from "./hash";
import { strToId, isEqual, type Id } from "./id";
import { EDGE, HEAD, TAIL } from "./ids";

const ALPHA = strToId('ALPHA');
const BETA = strToId('BETA');
const DELTA = strToId('DELTA');

describe("hash", () => {

  it("hashStringRaw(str: string)", () => {
    const id = hashStringRaw("Hello, World!");
    expect(id.toString("base64")).toEqual("3/1gIbsr1bCvZ2KQgJ7DpTGR3YHH9wpLKGiKNiGCmG8=");
  });

  it("hashNext", () => {
    let node = EDGE;
    let nodes: Id[] = [];

    for(let i=0; i<5; i++) {
      node = hashNext(node);
      nodes.push(node);
    }

    expect(nodes.map(node => node.base64)).toEqual([
      "d6Rt8fMEBxd02ahB6RLory8WHeIW1c5oI2go6OFYsko=",
      "tWdIgS7ytFOB08+LoogOzJS28hy+nOJro6b+69wgGnI=",
      "FpS31Yl3Bh1sqLfZ5TrXAoixBlI/PAK7uHjfBBqMNTg=",
      "UAERi1h8BKU3dFpxjzzE4BdDlRbSWgYg4P9XqsW9j1o=",
      "MvNFYSkk3N2D1y2nso0CMkVb7ETvb8iCHEKbE1UpQeg=",
    ]);
  });

  it("hashString(str: string)", () => {
    expect(hashString("Hello, World!").base64).toEqual("miAtYfUEEpLA1r+l3cL4Zt/f6Pc/EcI6v0O7mQ7zGLA=");
  });

  const myId = hash([EDGE, HEAD, TAIL]);

  it("hash(...ids)", () => {
    expect(myId.base64).toEqual('JrozwjYs4pNgvLaIhud5b0fh6NpEmderSdQPrHXZLBI=');
  });

  it("hashEdge({ head: Id, tail: Id })", () => {
    const edgeHash = hashEdge({ head: HEAD, tail: TAIL });
    expect(isEqual(myId, edgeHash)).toBe(true);
  });

  it("hashSet(set: Id[])", () => {
    const base64 = "QFa58EHsPxMZEPpdch5ruSRsrWhce1yTlbBfaItX4zg=";

    const setHash = hashSet([ALPHA, BETA, DELTA]);
    expect(setHash.base64).toEqual(base64);

    const setHashDuplicate = hashSet([ALPHA, BETA, BETA, DELTA]);
    expect(setHashDuplicate.base64).toEqual(base64);

    const setHashReordered = hashSet([DELTA, ALPHA, BETA]);
    expect(setHashReordered.base64).toEqual(base64);
  });

  it("hashList(set: Id[])", () => {
    const listHash = hashList([ALPHA, BETA, DELTA]);
    expect(listHash.base64).toEqual("5qHEXbC/HPnTkhns3AZJzXBcEj2Mlnwumv670+XAhZ4=");

    const listHashDuplicate = hashList([ALPHA, BETA, BETA, DELTA]);
    expect(listHashDuplicate.base64).toEqual("xKbn20EUXf78V4sNR7n2GaQRnObbu9oIyKM5QbCKNvA=");

    const listHashReordered = hashList([DELTA, ALPHA, BETA]);
    expect(listHashReordered.base64).toEqual("IAtjlK8uk+9DSKgl0+Ee0MmboGx48Gqmk3NI1b3JFjI=");
  });
});
