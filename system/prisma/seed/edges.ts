import { ARGUMENT_MAPPING, strToId } from "@kitsune-system/common";

import { edges as rawEdges, namedEdges, sets } from "../../src/data/edges";
import { argMappings } from "../../src/data/values";
import { createEdge } from "../../src/prisma/edge";
import { createString } from "../../src/prisma/string";
import { toEdges } from "../../src/utils/edge";

const seedArgMappings = () => Promise.all(Object.entries(argMappings).map(async ([fnIdStr, args]) => {
  const fnId = strToId(fnIdStr);
  const propId = await createEdge({ head: ARGUMENT_MAPPING, tail: fnId });

  await Promise.all(Object.entries(args).map(async ([argName, id]) => {
    const nameId = await createString(argName);
    const propValId = await createEdge({ head: propId, tail: nameId });
    createEdge({ head: propValId, tail: id });
  }));
}));

export const seedEdges = async () => {
  const { edges, names } = toEdges({
    edges: rawEdges,
    namedEdges,
    sets,
  });

  edges.forEach(createEdge);

  await seedArgMappings();

  // Log output
  const nameToIdStr = Object.fromEntries(
    Object.entries(names).map(([name, id]) => [name, id.base64])
  );

  console.log();
  console.log("Names:");
  console.log(nameToIdStr);

  // Write names to disk
  // fs.writeFileSync("./prisma/edges.json", JSON.stringify(names, null, 2));
};
