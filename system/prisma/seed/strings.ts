import { namedStrings, strings } from "../../src/data/strings";
import { createString } from "../../src/prisma/string";

export const seedStrings = async () => {
  await Promise.all([
    ...strings.map(createString),
    ...Object.values(namedStrings).map(createString),
  ]);
};
