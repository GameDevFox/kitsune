-- CreateTable
CREATE TABLE "Edge" (
    "id" BLOB NOT NULL PRIMARY KEY,
    "head" BLOB NOT NULL,
    "tail" BLOB NOT NULL
);

-- CreateTable
CREATE TABLE "Str" (
    "id" BLOB NOT NULL PRIMARY KEY,
    "string" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "Edge_head_tail_key" ON "Edge"("head", "tail");

-- CreateIndex
CREATE UNIQUE INDEX "Str_string_key" ON "Str"("string");
