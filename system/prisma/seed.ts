import { seedEdges } from "./seed/edges";
import { seedStrings } from "./seed/strings";

(async () => {
  await seedEdges();
  await seedStrings();
})();
