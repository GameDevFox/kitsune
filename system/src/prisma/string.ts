import { bufferToId, Id } from "@kitsune-system/common";

import { prisma } from "./prisma";
import { hashString } from "@kitsune-system/common";

export const createString = async (str: string) => {
  const id = hashString(str);

  const record = await prisma.str.upsert({
    where: { id: id.buffer },
    create: {
      id: id.buffer,
      string: str,
    },
    update: {},
  });

  return bufferToId(record.id);
}

export const getString = (id: Id) => prisma.str.findUnique({
  where: { id: id.buffer },
});

export const resolveString = async (id: Id) => {
  const str = await getString(id);
  return str?.string ?? null;
};
