import { Edge } from "@prisma/client";

import { bufferToId, hashEdge, Id } from "@kitsune-system/common";

import { prisma } from "./prisma";

interface EdgeWithIds {
  id: Id,
  head: Id,
  tail: Id,
};

const toEdgeWithIds = (object: Edge): EdgeWithIds => ({
  id: bufferToId(object.id),
  head: bufferToId(object.head),
  tail: bufferToId(object.tail),
});

export const createEdge = async (args: { head: Id, tail: Id }) => {
  const { head, tail } = args;

  const id = hashEdge({ head, tail })

  const record = await prisma.edge.upsert({
    where: { id: id.buffer },
    create: {
      id: id.buffer,
      head: head.buffer,
      tail: tail.buffer,
    },
    update: {},
  });

  return bufferToId(record.id);
};

export const resolveEdge = async (id: Id) => {
  const edge = await prisma.edge.findUnique({
    where: { id: id.buffer },
  });

  return edge ? toEdgeWithIds(edge) : null;
};

export const listHeads = async (id: Id) => {
  const headEdges = await prisma.edge.findMany({
    where: { tail: id.buffer },
  });

  return headEdges.map(edge => bufferToId(edge.head));
};

export const listTails = async (id: Id) => {
  const tailEdges = await prisma.edge.findMany({
    where: { head: id.buffer },
  });

  return tailEdges.map(edge => bufferToId(edge.tail));
};

export const listHeadEdges = async (id: Id) => {
  const headEdges = await prisma.edge.findMany({
    where: { tail: id.buffer },
  });

  return headEdges.map(toEdgeWithIds);
};

export const listTailEdges = async (id: Id) => {
  const tailEdges = await prisma.edge.findMany({
    where: { head: id.buffer },
  });

  return tailEdges.map(toEdgeWithIds);
};
