import { hashEdge, Id } from "@kitsune-system/common";
import { ARGUMENT_MAPPING } from "@kitsune-system/common";

import { listTailEdges, listTails } from "./prisma/edge";
import { resolveString } from "./prisma/string";

export const mapValuesToStrings = async (mappings: Map<Id, Id>) => {
  const nameMappings: Record<string, string> = {};
  for(const [nameId, value] of Array.from(mappings)) {
    const name = await resolveString(nameId);
    if(!name)
      throw new Error(`Can't find name. No "string" for id: ${nameId.base64}`);

    nameMappings[value.base64] = name;
  }

  return nameMappings;
}

export const resolveMap = async (id: Id) => {
  const result: Map<Id, Id> = new Map();

  const tailEdges = await listTailEdges(id);

  await Promise.all(tailEdges.map(async tailEdge => {
    const { tail: key } = tailEdge;

    const tails = await listTails(tailEdge.id);
    if(tails.length !== 1)
      throw new Error(`Malformed graph map, key with zero or more than one value: ${id.base64}`);

    result.set(key, tails[0]);
  }));

  return result;
};


export const buildArgMapping = async (fnId: Id) => {
  const mapId = hashEdge({ head: ARGUMENT_MAPPING, tail: fnId });

  const rawMappings = await resolveMap(mapId);
  const argMappings = await mapValuesToStrings(rawMappings);

  return argMappings;
};

export const renameKeys = (
  object: Record<string, string>,
  mapping: Record<string, string>,
) => {
  const mappedArgs: Record<string, string> = {};
  Object.entries(object).forEach(([key, value]) => {
    const name = mapping[key];
    if(!name)
      return;

    mappedArgs[name] = value;
  });

  return mappedArgs;
};

export const buildArgMapper = async (mapping: Record<string, string>) => {
  return (args: Record<string, string>) => renameKeys(args, mapping);
};

export const buildArgMapperById = async (fnId: Id) => {
  const mapping = await buildArgMapping(fnId);
  return buildArgMapper(mapping);
};
