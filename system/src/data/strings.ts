import { hashString, Id } from "@kitsune-system/common";

export const strings = [
  "Hello, World!",
];

export const namedStrings = {
  alphaAndOmega: "Alpha and Omega",
};

export const stringNodes = Object.fromEntries(Object.entries(namedStrings)
  .map(([key, value]) => [key, hashString(value)])) as Record<keyof typeof namedStrings, Id>;
