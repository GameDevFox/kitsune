import { Id } from "@kitsune-system/common";

import {
  ARGUMENT_MAPPED_FN, ARGUMENT_MAPPING, FN_ADD_EDGE, FN_BUILD_ARG_MAPPER,
  FN_BUILD_ARG_MAPPING, FN_MAP_VALUES_TO_STRINGS, FN_RESOLVE_MAP, HEAD,
  ID, PRIMARY_INPUT, SET, TAIL, Z_BUILD_GREET, Z_MY_NAME, Z_YOUR_NAME,
} from "@kitsune-system/common";

import { EntryEdge } from "../utils/edge";

import { stringNodes } from "./strings";

export const edges: EntryEdge[] = [
  [Z_BUILD_GREET, Z_MY_NAME],
  [Z_BUILD_GREET, Z_YOUR_NAME],
];

// TODO: Direct reference
export const namedEdges: Record<string, EntryEdge> = {
  loaderTest: [ID, stringNodes.alphaAndOmega],

  str2Id: [ID, FN_ADD_EDGE],
  callArgMapperWithId: [FN_BUILD_ARG_MAPPING, 'str2Id'],

  mappedFn: [ARGUMENT_MAPPED_FN, FN_ADD_EDGE],

  argMappingNode: [ARGUMENT_MAPPING, FN_ADD_EDGE],
  argMaddingId: [ID, 'argMappingNode'],
  argMapping: [FN_RESOLVE_MAP, 'argMaddingId'],
  argMappingWithStrings: [FN_MAP_VALUES_TO_STRINGS, 'argMapping'],
  argMapperFn: [FN_BUILD_ARG_MAPPER, 'argMappingWithStrings'],
  callArgMapperFn: ['argMapperFn', PRIMARY_INPUT],
};

export const sets: Record<string, Id[]> = {
  [SET.base64]: [
    SET,
    HEAD,
    TAIL,
    Z_MY_NAME,
    Z_YOUR_NAME,
  ],
};

const graph = {
  "$edges": edges,
  "$sets": sets,

  ...namedEdges,
};
