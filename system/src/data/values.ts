import { Id, strToId } from "@kitsune-system/common";

import {
  FN_ADD_EDGE, FN_BUILD_ARG_MAPPER, FN_BUILD_ARG_MAPPING, FN_BUILD_MAP_ARGS_FN,
  FN_IDENTITY, FN_LIST_TAILS, FN_MAP_VALUES_TO_STRINGS, FN_RESOLVE_MAP,
  FN_STRING_TO_ID, HEAD, TAIL, Z_ARGS_BY_ID, Z_BUILD_GREET, Z_MY_NAME, Z_YOUR_NAME,
} from '@kitsune-system/common';

import { createEdge, listTails } from '../prisma/edge';
import { Resolver } from "../resolve/resolve";
import { buildArgMapper, buildArgMapperById, buildArgMapping, mapValuesToStrings, resolveMap } from "../values";
import { addNode, addNodeFn } from "../utils/value";

export const nodes: Record<string, any> = {};
export const argMappings: Record<string, Record<string, Id>> = {};

addNode(FN_BUILD_ARG_MAPPER, buildArgMapper);
addNode(FN_BUILD_ARG_MAPPING, buildArgMapping);
addNode(FN_MAP_VALUES_TO_STRINGS, mapValuesToStrings);
addNode(FN_RESOLVE_MAP, resolveMap);
addNode(FN_STRING_TO_ID, strToId);

addNode(FN_IDENTITY, (input: any) => input);

addNodeFn(FN_ADD_EDGE, async ({ head, tail }) => {
  const id = await createEdge({
    head: strToId(head),
    tail: strToId(tail)
  });

  return id;
}, { head: HEAD, tail: TAIL });

// TODO: Fix this. Function signature is different from arg mappings
addNode(Z_BUILD_GREET, (name: string) => {
  return () => `Hello, ${name}!`;
});

addNode(Z_MY_NAME, "Edward");
addNode(Z_YOUR_NAME, "Evelyn");

addNode(Z_ARGS_BY_ID, async (args: any) => {
  const head = args[HEAD.base64];
  const tail = args[TAIL.base64];

  const msg = `==== ${head} ${tail}`;

  return msg;
});

addNode(FN_BUILD_MAP_ARGS_FN, async (fnId: Id, resolve: Resolver) => {
  const argMapper = await buildArgMapperById(fnId);
  const fn = await resolve(fnId);

  const result = (args: Record<string, string>) => {
    const mappedArgs = argMapper(args);
    return fn(mappedArgs);
  };

  return result;
});

addNode(FN_LIST_TAILS, listTails);
