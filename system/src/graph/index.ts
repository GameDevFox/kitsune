import { Id } from "@kitsune-system/common";
import {
  ARGUMENT_MAPPED_FN, FN_BUILD_MAP_ARGS_FN, FN_IDENTITY,
  FN_RESOLVE_MAP, ID, MAP,
} from "@kitsune-system/common";

import { resolveEdge } from "../prisma/edge";
import { Resolver } from "../resolve/resolve";

type ResolveRef = { resolve: Resolver };

// TODO: Load this from graph?
const loaders: Record<string, Id> = {
  [ID.base64]: FN_IDENTITY,
  [ARGUMENT_MAPPED_FN.base64]: FN_BUILD_MAP_ARGS_FN,
  // TODO: Change to MAP
  [MAP.base64]: FN_RESOLVE_MAP,
};

export const GraphResolver = () => {
  const resolveRef: ResolveRef = { resolve: () => null }

  const resolve: Resolver = async (id) => {
    const edge = await resolveEdge(id);
    if(!edge)
      return null;

    const { resolve } = resolveRef;

    const { head, tail } = edge;

    // Use Loader is possible
    let result;
    if(head.base64 in loaders) {
      const loaderId = loaders[head.base64];
      const loader = await resolve(loaderId);
      result = await loader(tail, resolveRef.resolve);
    } else {
      const fn = await resolve(head);
      const args = await resolve(tail);

      result = await fn(args);
    }

    return result;
  };

  return { resolve, resolveRef };
};
