import { Id } from "@kitsune-system/common";

import { GraphResolver } from "../graph";
import { resolveString } from "../prisma/string";
import { nodes } from "../data/values";

export type Resolver = (id: Id) => any;

export const SerialResolver = (resolvers: Resolver[]): Resolver => {
  return async id => {
    for(const resolver of resolvers) {
      const result = await resolver(id);
      if(result != null)
        return result;
    }

    return null;
  };
};

export const ParallelResolver = (resolvers: Resolver[]): Resolver => {
  return async id => {
    let count = 0;

    return new Promise(res => {
      resolvers.map(async resolve => {
        const result = await resolve(id);

        if(result == null)
          count++;
        else
          res(result);

        if(count === resolvers.length)
          res(null);
      });
    });
  };
};

export const ObjectResolver = (obj: Record<string, any>): Resolver => {
  return async id => {
    const { base64 } = id;

    if(base64 in obj)
      return obj[base64];

    return null;
  };
};

const resolveValue = ObjectResolver(nodes);

// Resolver pattern
// 1 - Args (Object Resolver)
// 2 - Values (Object Resolver)
// 3 - {
//   * Graph Resolver
//   * String Resolver
// }

export const buildResolver = (args: any) => {
  const resolveArg = ObjectResolver(args);

  const {
    resolve: resolveGraph,
    resolveRef: graphResolveRef
  } = GraphResolver();

  const level2Resolve = ParallelResolver([resolveGraph, resolveString]);

  const resolve = SerialResolver([
    resolveArg, resolveValue, // level 1
    level2Resolve,
  ]);

  // Link the graph resolver ref
  // This enables recursive "resolve"-ing in the GraphResolver
  graphResolveRef.resolve = resolve;

  return resolve;
};
