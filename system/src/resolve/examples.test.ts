import { hashString, strToId } from "@kitsune-system/common";
import { hashEdge, hashEdges, IdOrEdge } from "@kitsune-system/common";

import {
  ARGUMENT_MAPPED_FN, ARGUMENT_MAPPING, FN_ADD_EDGE, FN_BUILD_ARG_MAPPER,
  FN_BUILD_ARG_MAPPING, FN_MAP_VALUES_TO_STRINGS, FN_RESOLVE_MAP,
  PRIMARY_INPUT, HEAD, ID, TAIL, Z_ARGS_BY_ID, Z_MY_NAME, Z_YOUR_NAME,
} from "@kitsune-system/common";

import { call, greet, resolve } from "../http/graphql";

const ALPHA_AND_OMEGA = hashString("Alpha and Omega");

describe("Examples", () => {
  it("should work", () => {
    const msg = greet("Edward");
    expect(msg).toBe("Hello, Edward!");
  });

  it("resolve(STRING)", async () => {
    // Loads value directly from "values.ts"
    let result = await resolve(Z_MY_NAME);
    expect(result).toBe("Edward");

    result = await resolve(Z_YOUR_NAME);
    expect(result).toBe("Evelyn");

    // Loads a string from the string table
    result = await resolve(ALPHA_AND_OMEGA);
    expect(result).toBe("Alpha and Omega");

    result = await resolve(hashString("Alpha and Omega"));
    expect(result).toBe("Alpha and Omega");
  });

  it("resolve(MISSING STRING)", async () => {
    const result = await resolve(strToId("MISSING FROM STRING DB"));
    expect(result).toBe(null);
  });

  it("resolve([ID, alphaOmegaStr])", async () => {
    // Loads an Id through the FN_IDENTITY Loader which returns the Id on it's own

    // From db:seed - loaderTest
    const loaderTest = hashEdge({ head: ID, tail: ALPHA_AND_OMEGA });
    const result = await resolve(loaderTest);

    expect(result).toHaveProperty("base64");
    expect(result).toHaveProperty("buffer");
    expect(result).toHaveProperty("serial");

    expect(result).toBe(ALPHA_AND_OMEGA);
  });

  it("call(Z_ARGS_BY_ID, { ... })", async () => {
    // Calls function with two different forms of arguments
    let result = await call(Z_ARGS_BY_ID, {
      [HEAD.base64]: "ALPHA",
      [TAIL.base64]: "OMEGA",
    });
    expect(result).toBe("==== ALPHA OMEGA");

    result = await call(Z_ARGS_BY_ID, {
      "hello": "ALPHA",
      "world": "OMEGA",
    });
    expect(result).toBe("==== undefined undefined");
  });

  it("resolve([FN_BUILD_ARG_MAPPING, [ID, FN_ADD_EDGE]])", async () => {
    // Return an object that returns the ID mapping for a function
    // in ID -> STR format.
    // This is so that we can map plain objects indexed by string
    // into the the ID format which are objects indexed by ID.

    // Example result from this test:
    // {
    //   "cS+ulEL1KWhdrex4qvn5CuN9TvocNd2n0cdT/YrEe0c=": "count",
    //   "Fuu0nk+n+0/z0+rvaqtqFoHz4u2xwPjfNflIBho39RY=": "entry",
    // }

    // Example mapping input (via result above):
    // {
    //   "count": 123,
    //   "entry": "hello",
    // }

    // Example mapping output (via result above):
    // {
    //   "cS+ulEL1KWhdrex4qvn5CuN9TvocNd2n0cdT/YrEe0c=": 123,
    //   "Fuu0nk+n+0/z0+rvaqtqFoHz4u2xwPjfNflIBho39RY=": "hello",
    // }
    // This object is used as the argument to calling pure functions
    // in the system.

    // From db:seed - callArgMapperWithId
    const callArgMapperWithId = hashEdges([
      FN_BUILD_ARG_MAPPING,
      [ID, FN_ADD_EDGE],
    ]);

    const result = await resolve(callArgMapperWithId);
    expect(result).toEqual({
      [HEAD.base64]: "head",
      [TAIL.base64]: "tail",
    });
  });

  // TODO: This unit test ISN'T a unit test since
  // it adds records to the `edge` table in the DB.
  // Fix it!
  it("call([ARGUMENT_MAPPED_FN, FN_ADD_EDGE])", async () => {
    // Calls FN_ADD_EDGE with arguments that are from
    // an ID keyed object to a STRING keyed object.

    // From db:seed - mappedFn
    const mappedFn = hashEdges([ARGUMENT_MAPPED_FN, FN_ADD_EDGE]);

    let result = await call(mappedFn, {
      [HEAD.base64]: "ALPHA",
      [TAIL.base64]: "OMEGA",
    });

    const edgeId = hashEdges([strToId("ALPHA"), strToId("OMEGA")]);
    expect(result).toBe(edgeId);
  });

  it("resolve(*metaFnCallResult*)", async () => {
    // Call a function via resolve by chaining multiple
    // edges together in a graph

    const FN_ADD_EDGE_MAPPED_ID: IdOrEdge = [
      ID,
      [ARGUMENT_MAPPING, FN_ADD_EDGE],
    ];

    const FN_MAP_ID_KEYS_TO_STRING_KEYS: IdOrEdge = [
      FN_BUILD_ARG_MAPPER,
      [
        FN_MAP_VALUES_TO_STRINGS,
        [
          FN_RESOLVE_MAP,
          FN_ADD_EDGE_MAPPED_ID,
        ],
      ],
    ];

    // From db:seed - callArgMapperFn
    const callArgMapperFn = hashEdges([FN_MAP_ID_KEYS_TO_STRING_KEYS, PRIMARY_INPUT]);

    const result = await resolve(callArgMapperFn, {
      [PRIMARY_INPUT.base64]: {
        [HEAD.base64]: "ALPHA",
        [TAIL.base64]: "OMEGA",
      },
    });

    expect(result).toEqual({
      "head": "ALPHA",
      "tail": "OMEGA",
    });
  });
});
