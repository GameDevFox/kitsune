import { readFileSync } from "fs";

import GraphQLJSON from "graphql-type-json";

import { ApolloServer } from "@apollo/server";
import { Id, strToId } from "@kitsune-system/common";

import { Resolvers } from '../../generated-types/graphql';

import { buildResolver } from "../resolve/resolve";

export const greet = (name: string) => `Hello, ${name}!`;

export const resolve = (id: Id, values: any = {}) => {
  const resolve = buildResolver(values);
  return resolve(id);
};

export const call = async (id: Id, args: any = {}) => {
  const resolve = buildResolver(args);

  const fn = await resolve(id);
  if(typeof fn !== "function")
    throw new Error(`Node "${id}" does not represent a function`);

  // TODO: Shouldn't use `args` here. Sanitize the input somehow
  const result = await fn(args);

  return result;
};

const resolvers: Resolvers  = {
  JSON: GraphQLJSON,

  Query: {
    greet: async (_parent, args) => {
      const { name } = args;
      return greet(name);
    },
  },

  Mutation: {
    resolve: async (_parent, callArgs) => {
      const id = strToId(callArgs.id);
      const values = callArgs.values ?? {};

      // Resolve value
      const result = await resolve(id, values);

      return result;
    },

    call: async (_parent, callArgs) => {
      const id = strToId(callArgs.id);
      const args = callArgs.args ?? {};

      // Resolve function
      const result = call(id, args);

      return result;
    },
  }
};

const typeDefs = readFileSync(`${__dirname}/schema.graphql`, "utf8");

export interface Context {
  hello: string,
};

export const context =  async (): Promise<Context> => ({ hello: "world" });

export const server = new ApolloServer<Context>({
  typeDefs,
  resolvers,
});
