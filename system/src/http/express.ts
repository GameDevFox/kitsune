import bodyParser from "body-parser";
import cors from "cors";
import express from "express";

import { nodes } from "../data/values";

import { greet } from "./graphql";

const app = express();

app.use(cors())
app.use(bodyParser.text());

app.get("/greet/:name", (req, res) => {
  const { name } = req.params;

  const msg = greet(name);
  const result = { msg };
  res.send(result);
});

app.get("/call/:id", async (req, res) => {
  const { id } = req.params;

  if(!(id in nodes))
    return res.sendStatus(404);

  const fn = nodes[id];
  const result = await fn();

  res.send({ id, result });
});

app.post("/log", (req, res) => {
  const { body } = req;
  console.log(body);
  res.sendStatus(200);
});

export default app;
