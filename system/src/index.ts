import { startStandaloneServer } from "@apollo/server/standalone";

import app from "./http/express";
import { server, context } from "./http/graphql";

const restPort = 8080;

(async () => {
  app.listen(8080);
  console.log(`🚀 REST Server ready at port: ${restPort}`);

  const { url } = await startStandaloneServer(server, {
    listen: { port: 4000 },
    context,
  });

  console.log(`🚀 GraphQL Server ready at: ${url}`);
})();
