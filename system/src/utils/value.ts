import { Id } from "@kitsune-system/common";

import { argMappings, nodes } from "../data/values";

export const addNode = (id: Id, value: any) => {
  nodes[id.base64] = value;
};

type NodeFn = (args: Record<string, any>) => any;

export const addNodeFn = (id: Id, fn: NodeFn, argMapping: Record<string, Id> = {}) => {
  const valueMap: Record<string, boolean> = {};

  // Verify that no Id is mapped more than once
  Object.values(argMapping).forEach(value => {
    const { base64 } = value;
    if(base64 in valueMap)
      throw new Error(`The Id "${base64}" is mapped more than once`);

    valueMap[base64] = true;
  });

  addNode(id, fn);
  argMappings[id.base64] = argMapping;
};
