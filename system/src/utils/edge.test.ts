import { FUNCTION, GRAPH, HEAD, LIST, MAP, NODE, SET, STRING, TAIL } from "@kitsune-system/common";

import { sortEdges, toEdges } from "./edge";
import { hashEdges } from "@kitsune-system/common";

describe("toEdges", () => {
  it("should work", () => {
    const { edges, names } = toEdges({
      edges: [
        [LIST, MAP],
        [NODE, "alpha"],
        ["beta", "omega"],
      ],

      namedEdges: {
        "alpha": [HEAD, TAIL],
        "beta": [STRING, "alpha"],
        "omega": ["alpha", "beta"],
      },

      sets: {
        [SET.base64]: [
          SET,
          GRAPH,
          FUNCTION,
          "alpha",
          "omega",
        ],
      },
    });

    expect(sortEdges(edges)).toEqual(sortEdges([
      // Edges
      [LIST, MAP],
      [NODE, hashEdges([HEAD, TAIL])],
      [
        hashEdges([STRING, [HEAD, TAIL]]),
        hashEdges([
          [HEAD, TAIL],
          [STRING, [HEAD, TAIL]]
        ])
      ],

      // Named edges
      [HEAD, TAIL],
      [STRING, hashEdges([HEAD, TAIL])],
      [
        hashEdges([HEAD, TAIL]),
        hashEdges([STRING, [HEAD, TAIL]])
      ],

      // Sets
      [SET, SET],
      [SET, GRAPH],
      [SET, FUNCTION],
      [SET, hashEdges([HEAD, TAIL])],
      [SET, hashEdges([
        [HEAD, TAIL],
        [STRING, [HEAD, TAIL]],
      ])],
    ].map(([head, tail]) => ({ head, tail }))));

    expect(names).toEqual({
      "alpha": hashEdges([HEAD, TAIL]),
      "beta": hashEdges([STRING, [HEAD, TAIL]]),
      "omega": hashEdges([
        [HEAD, TAIL],
        [STRING, [HEAD, TAIL]]
      ])
    });
  });
});
