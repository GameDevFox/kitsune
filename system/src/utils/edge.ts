import { hashEdge, Id, strToId, compare as compareId, Edge } from "@kitsune-system/common";

export type Entry = Id | string;
export type EntryEdge = [Entry, Entry];

export const setsToEdges = (sets: Record<string, Id[]>) => {
  const setEdges: [Id, Id][] = [];
  Object.entries(sets).forEach(([setIdStr, tailIds]) => {
    const setId = strToId(setIdStr);

    tailIds.forEach(tailId => {
      setEdges.push([setId, tailId])
    });
  });

  return setEdges;
};

interface ToEdges {
  edges: EntryEdge[],
  namedEdges: Record<string, EntryEdge>,
  sets: Record<string, Entry[]>,
}

export const toEdges = (graph: ToEdges) => {
  const { edges: rawEdges, namedEdges, sets } = graph;

  const edges: Record<string, Edge> = {}
  const names: Record<string, Id> = {};

  const entryToId = (entry: Entry) => {
    if(typeof entry !== "string")
      return entry;

    if(entry in names)
      return names[entry];

    if(!(entry in namedEdges))
      throw new Error(`No "${entry}" entry in namedEdges`);

    const entryEdge = namedEdges[entry];
    const id = entryEdgeToId(entryEdge);

    names[entry] = id;

    return id;
  };

  const entryEdgeToId = (entryEdge: EntryEdge) => {
    const [head, tail] = entryEdge.map(entry => entryToId(entry));

    const id = hashEdge({ head, tail });
    edges[id.base64] = { head, tail };

    return id;
  }

  // Edges
  rawEdges.forEach(entryEdgeToId);

  // Named edges
  Object.entries(namedEdges as Record<string, EntryEdge>).forEach(([name, entryEdge]) => {
    names[name] = entryEdgeToId(entryEdge);
  });

  // Set edges
  Object.entries(sets as Record<string, Entry[]>).forEach(([setIdStr, tailIds]) => {
    const setId = strToId(setIdStr);
    tailIds.forEach(tailId => entryEdgeToId([setId, tailId]));
  });

  return { edges: Object.values(edges), names };
};

export const sortEdges = (edges: Edge[]) => {
  return edges.sort((a, b) => {
    const headCompare = compareId(a.head, b.head);

    if(headCompare !== 0)
      return headCompare;

    const tailCompare = compareId(a.tail, b.tail);
    return tailCompare;
  });
};
