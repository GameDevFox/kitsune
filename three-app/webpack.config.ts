import path from "path";
import HtmlWebpackPlugin from "html-webpack-plugin";
import { Configuration } from "webpack";
import { Configuration as DevServerConfiguration } from "webpack-dev-server";

// const config: Configuration = {
const config: Configuration & { devServer: DevServerConfiguration } = {
  mode: "development",

  entry: "./src/index.ts",

  devtool: "inline-source-map",
  devServer: {
    port: 3000,
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [ ".tsx", ".ts", ".js" ],
  },

  plugins: [
    new HtmlWebpackPlugin({
     template: "./src/index.html",
    }),
  ],

  output: {
    filename: "[name].bundle.js",
    path: path.resolve(__dirname, "dist"),
    clean: true,
  },
};

module.exports = config;
