import { Builder } from "./builder";
import { log } from "./log";
import { App, config } from "./webgl/app";

declare global {
  interface Window {
    kitsune: Awaited<ReturnType<typeof Builder<App>>>,
  }
}

(async () => {
  const app = await Builder(config);
  window.kitsune = app;

  console.log("App", app);

  const { renderer, animate } = app.data;
  renderer.setAnimationLoop(animate);

  log("");
  log(`=== Started: ${new Date()} ===`)
})();
