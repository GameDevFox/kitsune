import { BuilderFn } from "./builder";

import { App } from "./webgl/app";

export const hotModule = <T extends keyof App>(name: T, value: BuilderFn<App, any>) => {
  if(window.kitsune) {
    window.kitsune.update({ [name]: value }).then(() => {
      const { renderer, animate } = window.kitsune.data;
      renderer.setAnimationLoop(animate);
    });
  }
};
