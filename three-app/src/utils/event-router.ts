import _ from "lodash";

export const EventRouter = () => {
  const result = {
    router: {} as Record<string, any>,
    overlays: [] as string[],

    trigger: (id: string[], ...eventArgs: any[]) => {
      const { router, overlays } = result;

      const process = (prefix: string[]) => {
        const path = prefix.concat(id);
        const fn = _.get(router, path);

        if(fn) {
          fn(...eventArgs);
          return true;
        }

        return false;
      };

      let match = false;
      for(let i = overlays.length - 1; i >= 0; i--) {
        const overlay = overlays[i];

        match = process(["_overlays", overlay]);
        if(match)
          break;
      }

      if(!match)
        match = process([]);

      if(!match)
        result.default(id, eventArgs);
    },
    default: (id: string[], eventArgs: any[]) => {},
  };

  return result;
};

export const replaceOverlay = (overlays: string[], prefix: string, overlay: string) => {
  const index = overlays.findIndex(overlay => overlay.startsWith(prefix));

  if(index === -1)
    overlays.push(overlay);
  else
    overlays.splice(index, 1, overlay);
};

export const enableOverlay = (overlays: string[], overlay: string, enabled: boolean) => {
  const index = overlays.indexOf(overlay);
  if(index === -1)
    overlays.push(overlay);
  else
    overlays.splice(index, 1);
};

// const myRouter = EventRouter();

// // TODO: Move to unit test once unit testing is supported
// myRouter.router = {
//   left: {
//     a: () => console.log("<<< [[A]] was pressed!"),
//     b: () => console.log("<<< --B-- was pressed!"),
//   },
//   right: {
//     a: () => console.log("RIGHT [[A]] was pressed!"),
//     b: () => console.log("RIGHT --B-- was pressed!"),
//   },
//   _overlays: {
//     switched: {
//       right: {
//         a: () => console.log("RIGHT --B-- was pressed!"),
//         b: () => console.log("RIGHT [[A]] was pressed!"),
//         c: () => console.log("TODO: Please implement me"),
//       },
//     },
//     bonus: {
//       right: {
//         c: () => console.log("I actually exist now"),
//       },
//     },
//   }
// };

// myRouter.default = id => {
//   console.log("Count not trigger handler for", id);
// };

// myRouter.trigger(["left", "a"]);
// myRouter.trigger(["right", "b"]);
// myRouter.trigger(["right", "c"]);
// myRouter.trigger(["right", "d"]);

// console.log("========= switched ============");
// myRouter.overlays = ["switched"];

// myRouter.trigger(["left", "a"]);
// myRouter.trigger(["right", "b"]);
// myRouter.trigger(["right", "c"]);
// myRouter.trigger(["right", "d"]);

// console.log("========== bonus ===========");
// myRouter.overlays = ["bonus"];

// myRouter.trigger(["left", "a"]);
// myRouter.trigger(["right", "b"]);
// myRouter.trigger(["right", "c"]);
// myRouter.trigger(["right", "d"]);

// console.log("========== switched bonus ===========");
// myRouter.overlays = ["switched", "bonus"];

// myRouter.trigger(["left", "a"]);
// myRouter.trigger(["right", "b"]);
// myRouter.trigger(["right", "c"]);
// myRouter.trigger(["right", "d"]);

// console.log("========== bonus switched ===========");
// myRouter.overlays = ["bonus", "switched"];

// myRouter.trigger(["left", "a"]);
// myRouter.trigger(["right", "b"]);
// myRouter.trigger(["right", "c"]);
// myRouter.trigger(["right", "d"]);
