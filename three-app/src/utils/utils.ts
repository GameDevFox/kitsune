export const EventFilter = <T>(
  filter: (value: T) => boolean,
  output: (value: T) => void,
) => {
  const functions = {
    input: (value: T) => {
      const pass = functions.filter(value)
      if(pass)
        functions.output(value);
    },
    filter,
    output,
  };

  return functions;
};

export const OnChange = <T>(output: (value: T) => void, initial: T) => {
  let value: T = initial;

  const eventFilter = EventFilter<T>(
    newValue => {
      const result = value !== newValue;
      value = newValue;
      return result;
    },
    output,
  );

  return {
    ...eventFilter,
    get: () => value,
  };
}

export const bisect = (min: number, max: number, fn: (num: number) => number) => {
  if(min === max)
    return min;

  // Make sure min and max are in correct order
  if(max < min) {
    const temp = min;
    min = max;
    max = temp;
  }

  for(let i = 0; i < 100; i++) {
  // while(true) {
    const mid = Math.floor(((max - min) / 2) + min);
    const check = fn(mid);

    if(check == 0)
      return mid;

    if(check < 0) {
      // Exceptional cases
      if(mid === min)
        return Number.NEGATIVE_INFINITY;

      // Normal case
      max = mid;
    } else {
      // Exceptional cases
      if(min + 1 === max) {
        const check = fn(max);

        if(check === 0)
          return max;

        const result = check < 0 ? min : Number.POSITIVE_INFINITY;
        return result;
      }

      // Normal case
      min = mid;
    }
  };

  console.error(`Too deep! ${JSON.stringify({ min, max }, null, 2)}`);
  return min;
};

export const lookup = (record: Record<string, any>, value: any) => {
  const entry = Object.entries(record).find(([_, lookupValue]) => lookupValue === value);

  if(entry === undefined)
    return undefined;

  // Return the `key` from the entry
  return entry[0];
};

// NOTE: array elements must all be unique
// for this to work properly
export const getNextCircular = <T>(array: T[], value: T, previous: boolean = false) => {
  const index = array.indexOf(value);
  if(index === -1) {
    return {
      index,
      value: undefined,
    };
  }

  let newIndex = -1;
  if(previous) {
    const isFirst = index === 0;
    newIndex = isFirst ? array.length - 1 : index - 1;
  } else {
    const isLast = index === array.length - 1;
    newIndex = isLast ? 0 : index + 1;
  }

  return {
    index: newIndex,
    value: array[newIndex],
  }
};
