import { Object3D } from "three";

import { ConfigValue } from "../builder";

import { vecToStr } from "../format";
import { log } from "../log";
import { enableOverlay, replaceOverlay } from "../utils/event-router";
import { getNextCircular } from "../utils/utils";

import { App, getTransform } from "../webgl/app";
import { hud } from "../webgl/objects/console";

export type ControlMode = "select" | "adjust";

export type TransformMode = "position" | "rotation" | "scale";
export const transformModes: TransformMode[] = ["position", "rotation", "scale"];

export type AxisMode = "x" | "y" | "z";
export const axisModes: AxisMode[] = ["x", "y", "z"];

export const ControlRouter: ConfigValue<App> = [
  ["controls", "events", "flipper", "sceneObjects", "state", "xr"],
  ({ controls, events, flipper, sceneObjects, state, xr }) => {
    const { getController } = controls;
    const { line, rightControllerGripRig } = sceneObjects;

    const setMode = (mode: ControlMode) => {
      state.mode = mode;
      replaceOverlay(events.overlays, "mode:", `mode:${mode}`);
    };
    setMode("select");

    const defaultRouter = {
      controllers: {
        left: {
          trigger: {
            pressed: () => getController("left", controller => {
              log("L Control Pos:", vecToStr(controller.position))
            }),
          },

          grip: {
            changed: (value: boolean) => {
              state.altMode = value;
              enableOverlay(events.overlays, "altMode", value);
            },
          },

          b: {
            pressed: () => {
              console.log("Reset!!");
              xr.getSession()?.end();
              window.location.reload();
            },
          },
        },
        right: {
          b: {
            changed: (value: boolean) => {
              hud.object.visible = value;
            },
          },
        },
      },
    };

    const modes = {
      axisMode: axisModes,
      transformMode: transformModes,
    };

    const setFlipper = (object: Object3D) => {
      const value = getTransform(object, state.transformMode, state.axisMode);
      flipper.set(value);
    };

    const changeMode = (modeName: string, increment: boolean) => {
      const currentMode = (state as any)[modeName];
      const allModes = (modes as any)[modeName];

      const { value } = getNextCircular(allModes, currentMode, !increment);
      if(value)
        (state as any)[modeName] = value;

      const { target } = state;
      if(target)
        setFlipper(target);
    };

    const setTarget = (target: Object3D) => {
      state.target = target;
      setFlipper(target);
    };

    const overlays = {
      altMode: {
        controllers: {
          left: {
            dir4: {
              left: () => setMode("adjust"),
              right: () => setMode("select"),
            },
          },
        },
      },

      "mode:select": {
        controllers: {
          right: {
            dir4: {
              up: () => {
                const { target } = state;
                if(!target)
                  return;

                if(target.parent)
                  setTarget(target.parent);
              },
              down: () => {
                const { target } = state;
                if(!target)
                  return;

                if(target.children.length ?? 0 > 0) {
                  const newTarget = target.children[0];
                  setTarget(newTarget);
                }
              },
              left: () => {
                const { target } = state;
                if(!target)
                  return;

                if(target.parent) {
                  const index = target.parent.children.findIndex(child => child === target);
                  const newIndex = index === 0 ? target.parent.children.length - 1 : index - 1;
                  const newTarget = target.parent.children[newIndex];

                  setTarget(newTarget);
                }
              },
              right: () => {
                const { target } = state;
                if(!target)
                  return;

                if(target.parent) {
                  const index = target.parent.children.findIndex(child => child === target);
                  const newIndex = index === target.parent.children.length - 1 ? 0 : index + 1;
                  const newTarget = target.parent.children[newIndex];

                  setTarget(newTarget);
                }
              },
            },

            trigger: {
              changed: (value: boolean) => {
                line.visible = value;
              },
            },
          },
        },
      },

      "mode:adjust": {
        controllers: {
          left: {
            dir4: {
              up: () => changeMode("transformMode", false),
              down: () => changeMode("transformMode", true),
              left: () => changeMode("axisMode", false),
              right: () => changeMode("axisMode", true),
            },
          },

          right: {
            dir4: {
              up: () => flipper.increment(),
              down: () => flipper.decrement(),
              left: () => flipper.incrementExponent(),
              right: () => flipper.decrementExponent(),
            },

            thumbstick: {
              pressed: () => flipper.round(),
            },

            grip: {
              pressed: () => {
                const { target } = state;
                if(!target)
                  return;

                const { parent } = target;
                if(parent === null)
                  return;

                getController("right", controller => {
                  rightControllerGripRig.attach(target);

                  state.drag = {
                    target,
                    parent,
                  };
                });
              },

              released: () => {
                const { drag } = state;
                if(!drag)
                  return;

                const { target, parent } = drag;
                parent.attach(target);

                state.drag = null;
              },
            },
          },
        },
      },
    };

    // Event Routers
    events.router = {
      ...defaultRouter,
      _overlays: overlays,
    };

    return events;
  }
];
