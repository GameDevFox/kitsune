// GamepadButton
export const gamepadButtonStr = (button: GamepadButton) => {
  if(button === undefined)
    return undefined;

  return {
    pressed: button.pressed,
    touched: button.touched,
    value: button.value,
  };
};

// Vector
export const vecToStr = (vec: any, fixed: number = 3) => "[" +
  ("w" in vec ? `${vec.w.toFixed(fixed)}, ` : '') +
  vec.x.toFixed(fixed) + ", " +
  vec.y.toFixed(fixed) +
  ("z" in vec ? `, ${vec.z.toFixed(fixed)}` : '') +
"]";
