export type ConfigKey<App> = (keyof App) & string;

export type BuilderFn<App, NAME extends ConfigKey<App>> = (data: App) => App[NAME] | Promise<App[NAME]>;
export type ConfigValue<App> = [ConfigKey<App>[], BuilderFn<App, ConfigKey<App>>];

export type Config<App> = Record<ConfigKey<App>, ConfigValue<App>>;

export type ConfigBuilder<App, name extends ConfigKey<App>> = Config<App>[name][1];

export const Builder = async <T>(config: Config<T>) => {

  const getDeps = (name: ConfigKey<T>, path: ConfigKey<T>[]) => {
    if(path.includes(name))
      throw new Error(`Circular dependency: ${path.join(" > ")} > ${name}`);

    let result: Record<ConfigKey<T>, boolean> = {} as any

    const configEntry: ConfigValue<T> = config[name as ConfigKey<T>];
    const [depList] = configEntry;

    depList.forEach(dep => {
      result[dep] = true;

      const subDeps = getDeps(dep, [ ...path, name]);
      result = { ...result, ...subDeps };
    });

    return result;
  };

  const depsByName: Record<ConfigKey<T>, string[]> = {} as any;
  Object.keys(config).forEach(name => {
    const depMap = getDeps(name as ConfigKey<T>, []);
    depsByName[name as ConfigKey<T>] = Object.keys(depMap);
  });

  // Reverse Dependencies
  const reverseDepsByName: Record<string, string[]> = {};
  const addReverseDep = (name: ConfigKey<T>, dep: ConfigKey<T>) => {
    let deps = reverseDepsByName[name];

    if(!deps) {
      deps = [];
      reverseDepsByName[name] = deps;
    }

    deps.push(dep);
  };

  // Fill in missing deps with empty list
  Object.keys(config).forEach(key => {
    if(!(key in reverseDepsByName))
      reverseDepsByName[key] = [];
  });

  Object.entries(depsByName).forEach(([name, depList]) => {
    (depList as ConfigKey<T>[]).forEach(dep => addReverseDep(dep, name as ConfigKey<T>));
  });

  // Data
  const data = {} as T;

  const isBuiltByName: Record<ConfigKey<T>, boolean> = {} as any;
  const build = async (name: ConfigKey<T>) => {
    if(isBuiltByName[name])
      return;

    const [deps, buildFn] = config[name];

    for(const name of deps)
      await build(name);

    (data as any)[name] = await buildFn(data);
    isBuiltByName[name] = true;
  };

  for(const key of Object.keys(config))
    await build(key as ConfigKey<T>)

  // Update
  const update = async (updates: Partial<Record<ConfigKey<T>, BuilderFn<T, any>>>) => {
    let rebuildList: string[] = [];
    Object.entries(updates).forEach(([name, fn]) => {
      // Update builder fn
      const entry = (config as any)[name];
      entry[1] = fn;

      const reverseDeps = reverseDepsByName[name] || [];
      rebuildList = [...rebuildList, name, ...reverseDeps];
    });

    const builtList: string[] = [];
    const rebuild = async (name: ConfigKey<T>) => {
      if(builtList.includes(name))
        return;

      const [deps, builderFn] = config[name];

      const filteredDeps = deps
        .filter(name => rebuildList.includes(name))
        .filter(name => !builtList.includes(name));
      for(const name of filteredDeps)
        await rebuild(name);

      (data as any)[name] = await builderFn(data);

      builtList.push(name);
    };

    for(const name of rebuildList)
      await rebuild(name as ConfigKey<T>)

    return [...builtList];
  };

  return { data, depsByName, reverseDepsByName, update };
};
