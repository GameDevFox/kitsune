// import { toAsync } from "@kitsune-system/common/src/utils"

import { Builder, Config } from "./builder";

describe("builder", () => {

  it("should work", async () => {
    interface TestBuild {
      b: boolean,
      msg: string,
      total: number,
      x: number,
      y: number,
    };

    const config: Config<TestBuild> = {
      b: [
        ["total"],
        // toAsync(({ total }) => total % 3 === 0),
        ({ total }) => total % 3 === 0,
      ],
      msg: [
        ["b", "total"],
        ({ b, total }) => `The total is ${total} and is${b ? "" : " NOT"} divisible by 3`,
      ],
      total: [
        ["x", "y"],
        ({ x, y }) => x + y,
      ],
      x: [[], () => 123],
      y: [[], () => 456],
    };

    const { data, depsByName, reverseDepsByName, update } = await Builder(config);

    expect(data).toEqual({
      b: true,
      msg: "The total is 579 and is divisible by 3",
      total: 579,
      x: 123,
      y: 456,
    });

    expect(depsByName).toEqual({
      b: ["total", "x", "y"],
      msg: ["b", "total", "x", "y"],
      total: ["x", "y"],
      x: [],
      y: [],
    });

    expect(reverseDepsByName).toEqual({
      b: ["msg"],
      msg: [],
      total: ["b", "msg"],
      x: ["b", "msg", "total"],
      y: ["b", "msg", "total"],
    });

    const rebuilt = await update({ x: () => 999 });
    expect(rebuilt).toEqual(["x", "total", "b", "msg"]);

    expect(data).toEqual({
      b: true,
      msg: "The total is 1455 and is divisible by 3",
      total: 1455,
      x: 999,
      y: 456,
    });

    const rebuilt2 = await update({
      // b: toAsync(({ total }) => total % 2 === 0),
      b: ({ total }) => total % 2 === 0,
      msg: ({ b, total }) => `The total is ${total} and is${b ? "" : " NOT"} divisible by 2`,
    });
    expect(rebuilt2).toEqual(["b", "msg"]);

    expect(data).toEqual({
      b: false,
      msg: "The total is 1455 and is NOT divisible by 2",
      total: 1455,
      x: 999,
      y: 456,
    });
  });

  it("should throw an Error when encountering a circular dependency", async () => {
    // A
    //   B
    //     D
    //   C
    //     D
    //     E
    //       A
    //       B

    interface TestBuild {
      a: number,
      b: number,
      c: number,
      d: number,
      e: number,
    };

    const config: Config<TestBuild> = {
      a: [
        ["b", "c"],
        ({ b }) => b * 2,
      ],
      b: [
        ["d"],
        ({ d }) => d * 3,
      ],
      c: [
        ["d", "e"],
        ({ d, e }) => (d + e) * 4,
      ],
      d: [
        [],
        () => 5,
      ],
      e: [
        ["a", "b"],
        ({ a, b }) => (a + b) * 6,
      ],
    };

    expect(async () => {
      await Builder(config);
    }).rejects.toThrow("Circular dependency: a > c > e > a");
  });
});
