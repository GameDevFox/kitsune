import { Box3, Clock, Object3D, Raycaster } from "three";

import { ConfigValue } from "../builder";

import { App } from "./app";
import { vecToStr } from "../format";
import { hotModule } from "../hot";
import { log } from "../log";

import { getNamedButtons, getUserData } from "../controls/controls";
import { FlyControls } from "../controls/fly-controls";
import { BoxPoints } from "./objects/wireframe";
import { clearLiveConsole, logLive } from "./objects/console";
import { getDepth } from "./utils/depth";

export const animate: ConfigValue<App> = [
  ["camera", "controls", "flipper", "renderer", "state", "sceneObjects", "xr"],
  ({ camera, controls, flipper, renderer, state, sceneObjects, xr }) => {
    const {
      getController, getControllerGrip, getGamepad,
    } = controls;

    const {
      scene,
      cube, line, wireframe,
      headsetRig,
      rightControllerRig,
      leftControllerGripRig, rightControllerGripRig,
    } = sceneObjects;

    const clock = new Clock();

    // TODO: Replace or improve this
    const flyControls = new FlyControls(camera, renderer.domElement);
    flyControls.rollSpeed = 1;

    let count = 0;

    const raycaster = new Raycaster();
    raycaster.near = 0.01;

    const updateRigs = () => {
      // Update Headset Rig
      const camera = xr.getCamera();
      headsetRig.position.setFromMatrixPosition(camera.matrixWorld);
      headsetRig.rotation.setFromRotationMatrix(camera.matrixWorld);

      // Update Controller Rigs
      const rigConfig: [
        typeof getController | typeof getControllerGrip,
        XRHandedness,
        Object3D,
      ][] = [
        [getController, "right", rightControllerRig],
        [getControllerGrip, "left", leftControllerGripRig],
        [getControllerGrip, "right", rightControllerGripRig],
      ];

      rigConfig.forEach(([getController, handedness, rig]) => {
        getController(handedness, controller => {
          rig.position.setFromMatrixPosition(controller.matrixWorld);
          rig.rotation.setFromRotationMatrix(controller.matrixWorld);
        });
      });
    };

    const excludeFromRaycast = [
      rightControllerRig,
      rightControllerGripRig,
      wireframe,
    ];

    const processMode = () => {
      const { mode, drag } = state;

      if(mode === "select") {
        if(drag) {
          // TODO: Deduplicate this
          const { target, parent } = drag;
          parent.attach(target);

          state.drag = null;
        }

        getGamepad("right", (gamepad, controller) => {
          const namedButtons = getNamedButtons(gamepad.buttons);
          if(namedButtons.trigger.pressed) {

            raycaster.setFromXRController(controller);
            const targets = scene.children.filter(object => {
              return !excludeFromRaycast.includes(object);
            });
            logLive("Targets", targets.length);
            const intersections = raycaster.intersectObjects(targets);
            logLive("Intersections:", intersections.length);
            if(intersections.length > 0)
              state.target = intersections[0].object;
          } else {
            line.visible = false;
          }
        });
      }

      if(mode === "adjust")
        line.visible = false;
    };

    const animate = () => {
      clearLiveConsole();

      let { mode, altMode, target, transformMode, axisMode } = state;

      logLive("Clock:", clock.elapsedTime.toFixed(3));
      logLive(`MODE: ${mode} [${altMode ? "ALT MODE" : ""}]`);
      // logLive("Overlays:", JSON.stringify(events.overlays));

      controls.update();

      // floor.visible = true;

      if(cube) {
        cube.rotation.x += 0.01;
        cube.rotation.y += 0.015;
      }

      if(renderer.xr.isPresenting) {
        updateRigs();
        processMode();

        getController("right", controller => {
          const userData = getUserData(controller);
          const { onButtons } = userData;

          if(onButtons.a.get())
            log((count++) + " Turbo!!!");
        });
      }

      if(target) {
        logLive(`Target ${transformMode}:`, vecToStr(target[transformMode]));

        const box = new Box3().setFromObject(target);

        const points = BoxPoints(box.min, box.max);
        wireframe.geometry.setFromPoints(points);
        wireframe.geometry.getAttribute("position").needsUpdate;
        wireframe.geometry.computeBoundingBox();
        wireframe.geometry.computeBoundingSphere();

        if(mode === "select") {
          const siblingCount = target.parent?.children.length ?? "N/A";
          const childrenCount = target.children.length;
          logLive(`[Depth, Siblings, Children]\n[${getDepth(target)}, ${siblingCount}, ${childrenCount}]`);
        }
      }

      if(mode === "adjust") {
        logLive("Axis:", axisMode);

        const lines = flipper.getLines();
        logLive(lines[0]);
        logLive(lines[1]);
      }

      const delta = clock.getDelta();
      flyControls.update(delta)

      renderer.render(scene, camera);
      // composer.render();
    };

    const animateWithErrorHandling = () => {
      try {
        animate();
      } catch(e: any) {
        log(`Error: ${e.message}`);
      }
    };

    return animateWithErrorHandling;
  },
];

const [_deps, buildAnimate] = animate;

hotModule("animate", buildAnimate);
if(module.hot) module.hot.accept();
