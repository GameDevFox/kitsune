import { Camera, Object3D, WebGLRenderer, WebXRManager } from "three";

import { Config } from "../builder";

import { EventRouter } from "../utils/event-router";
import { DigitFlipper } from "../utils/number-flipper";

import { animate } from "./animate";
import { Camera as AppCamera } from "./camera";
import { AxisMode, ControlMode, ControlRouter, TransformMode } from "../controls/control-router";
import { controls, Controls } from "../controls/controls";
import { installRenderer } from "./renderer";
import { sceneObjects, SceneObjects } from "./scene";
import { log } from "../log";

interface State {
  altMode: boolean,
  drag: {
    target: Object3D,
    parent: Object3D,
  } | null,
  mode: ControlMode,
  target: Object3D | undefined,
  transformMode: TransformMode,
  axisMode: AxisMode,
};

const initialState: State = {
  altMode: false,
  drag: null,
  mode: "select",
  target: undefined,
  transformMode: "position",
  axisMode: "x",
};

export interface App {
  animate: () => void,
  camera: Camera,
  controls: Controls,
  controlRouter: ReturnType<typeof EventRouter>,
  events: ReturnType<typeof EventRouter>,
  flipper: ReturnType<typeof DigitFlipper>,
  renderer: WebGLRenderer
  sceneObjects: SceneObjects,
  state: State,
  xr: WebXRManager,
};

export const getTransform = (object: Object3D, transform: TransformMode, axis: AxisMode) => {
  const t = object[transform];
  return t[axis];
};

export const setTransform = (object: Object3D, transform: TransformMode, axis: AxisMode, value: number) => {
  const t = object[transform];
  t[axis] = value;
};

// TODO: Fix the types
// We should have an object of `Record<string, fn>` which
// is where we should be getting the type for `App`
export const config: Config<App> = {
  animate,
  camera: [[], AppCamera],
  controls,
  controlRouter: ControlRouter,
  events: [[], EventRouter],
  flipper: [
    ["state"],
    ({ state }) => {
      const flipper = DigitFlipper();

      flipper.output = value => {
        let { target, transformMode, axisMode } = state;
        if(target)
          setTransform(target, transformMode, axisMode, value)
      };

      return flipper;
    },
  ],
  renderer: [[], installRenderer],
  sceneObjects,
  state: [[], () => initialState],
  xr: [
    ["renderer"],
    ({ renderer }) => {
      const { xr } = renderer;

      const onXRSession = (xrSession: XRSession) => {
        log("Listening to XRSession visibilitychange ...")
        xrSession.addEventListener("visibilitychange", () => {

          const { visibilityState } = xrSession;
          log("XRSession visibilitychange", visibilityState);

          if(visibilityState === "hidden") {
            log("Ending XRSession");
            xr.getSession()?.end();
          }
        });
      };

      xr.addEventListener("sessionstart", () => {
        const xrSession = xr.getSession();
        if(xrSession)
          onXRSession(xrSession);
      });

      return xr;
    }
  ]
};
