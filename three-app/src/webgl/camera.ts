import { PerspectiveCamera } from "three";
import { hotModule } from "../hot";

export const Camera = () => {
  const camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
  camera.position.y = 1;
  camera.position.z = 2;
  return camera;
};

hotModule("camera", Camera);
if(module.hot) module.hot.accept();
