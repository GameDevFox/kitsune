import { WebGLRenderer } from "three";
import { XRButton } from "three/examples/jsm/webxr/XRButton";

const alpha = true;

export const Renderer = () => {
  const renderer = new WebGLRenderer({
    alpha,
    antialias: true,
  });

  renderer.setSize(window.innerWidth, window.innerHeight);

  renderer.xr.enabled = true;

  return renderer;
};

export const installRenderer = () => {
  const renderer = Renderer();

  const div = document.getElementById("webgl");
  if(div === null)
    throw new Error("Couldn't find `div` with \"webgl\" id");
  div.appendChild(renderer.domElement);

  const xrButton = XRButton.createButton(
    renderer,
    { requiredFeatures: ["hand-tracking"] },
  );
  document.body.appendChild(xrButton);

  return renderer;
};
