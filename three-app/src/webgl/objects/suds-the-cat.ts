import { Image } from "./image";

const SIZE = 1;

export const SudsTheCat = () => {
  const { mesh: suds } = Image("./suds-the-cat.jpg");

  suds.scale.x = SIZE * (480 / 360);
  suds.scale.y = SIZE;

  suds.position.y = 2.2;
  suds.position.z = 2;

  suds.rotation.x = -Math.PI / 8;
  suds.rotation.y = Math.PI;

  return suds;
}
