import { Mesh, MeshBasicMaterial, PlaneGeometry, SRGBColorSpace, TextureLoader } from "three";

export const Image = (url: string) => {
  const geometry = new PlaneGeometry();
  const texture = new TextureLoader().load(url);
  texture.colorSpace = SRGBColorSpace;
  const material = new MeshBasicMaterial({ map: texture });
  const mesh = new Mesh(geometry, material);

  return { geometry, material, mesh };
}
