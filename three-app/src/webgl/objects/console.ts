import { toLog } from "../../log";

import { Display } from "./display";

const LINE_LIMIT = 14;

export const console = Display();
export const liveConsole = Display();
export const hud = Display({ width: 2000, zoom: 4 });

const linesByName: Record<string, string[]> = {
  console: [],
  live: [],
};

const setFnByName: Record<string, (...values: any[]) => void> = {
  console: console.set,
  live: liveConsole.set,
};

export const clearLiveConsole = () => {
  const { live: lines } = linesByName;
  lines.splice(0, lines.length);
};

export const consoleLogLines = (name: string) => (...values: any[]) => {
  const lines = linesByName[name];

  const message = values.map(toLog).join(" ");
  const newLines = message.split("\n");
  newLines.forEach(line => lines.push(line));

  // FIFO action
  if(lines.length > LINE_LIMIT)
    lines.splice(0, lines.length - LINE_LIMIT);

  const set = setFnByName[name];
  set(lines.join("\n"));
};

export const consoleLog = consoleLogLines("console");
export const logLive = consoleLogLines("live");
