import { DoubleSide } from "three";

import { Image } from "./image";

const ASPECT_RATIO = 5000 / 8000;
const SCALE = 1.75;

export const Aki = () => {
  const { material, mesh: aki } = Image("./aki.png");

  material.side = DoubleSide;
  material.transparent = true;

  aki.position.y = SCALE * 0.48;

  aki.scale.x = SCALE * ASPECT_RATIO;
  aki.scale.y = SCALE;

  return aki;
};
