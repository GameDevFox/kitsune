import { BoxGeometry, Mesh, MeshStandardMaterial, SRGBColorSpace, TextureLoader } from "three";

export const Crate = () => {
  const geometry = new BoxGeometry(1, 1, 1);

  const texture = new TextureLoader().load("./crate.gif");
  texture.colorSpace = SRGBColorSpace;
  const material = new MeshStandardMaterial({ map: texture });

  const cube = new Mesh(geometry, material);
  return cube;
};
