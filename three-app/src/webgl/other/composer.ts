
import { Camera, Scene, WebGLRenderer } from "three";

import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer";
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass";
import { GlitchPass } from "three/examples/jsm/postprocessing/GlitchPass";
import { OutputPass } from "three/examples/jsm/postprocessing/OutputPass";

export const MyComposer = (renderer: WebGLRenderer, scene: Scene, camera: Camera) => {
  const composer = new EffectComposer(renderer);
  const renderPass = new RenderPass(scene, camera);
  composer.addPass(renderPass);
  const glitchPass = new GlitchPass();
  composer.addPass(glitchPass);
  const outputPass = new OutputPass();
  composer.addPass(outputPass);

  return composer;
};
