import { CanvasTexture, DoubleSide, Mesh, MeshBasicMaterial, Object3D, PlaneGeometry } from 'three';
import { bisect } from '../utils/utils';
import { toLog } from '../log';

interface Config {
  border: string,
  centered: boolean,
  color: string,
  fontSize: string,
  spacing: number,
  spacingScale: number,
  wordWrap: boolean,
};

const defaultConfig: Config = {
  border: "",
  centered: false,
  color: "#fff",
  fontSize: "10px",
  spacing: 0,
  spacingScale: 1,
  wordWrap: true,
};

export const createText = (width: number, height: number, config: Partial<Config> = defaultConfig) => {
  const {
    border, centered, color, fontSize, spacing, spacingScale, wordWrap
  } = { ...defaultConfig, ...config };

	const canvas = document.createElement('canvas');
  canvas.width = width;
  canvas.height = height;
  canvas.setAttribute("style", "color: yellow;")

	const context = canvas.getContext('2d');
  if(context === null)
    throw new Error("Failed to get 2d context.`canvas.getContext('2d')` was null.");

  context.font = `${fontSize} monospace`
  context.fillStyle = color;
  context.textAlign = centered ? "center" : "left";
  context.textBaseline = "top";

  const str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  const metrics = context.measureText(str);
  const textHeight = metrics.actualBoundingBoxAscent + metrics.actualBoundingBoxDescent;
  const actualSpacing = textHeight * spacingScale + spacing;

	const texture = new CanvasTexture(canvas);

	const material = new MeshBasicMaterial({
		side: DoubleSide,
		map: texture,
		transparent: true,
	});

	const geometry = new PlaneGeometry(1, 1);

	const mesh = new Mesh( geometry, material );
  mesh.scale.x = 1;
  mesh.scale.y = height / width;

  const bisectWidth = (str: string, maxWidth: number) => bisect(1, str.length, num => {
    const subStr = str.slice(0, num);
    const { width } = context.measureText(subStr);

    return maxWidth - width;
  });

  // TODO: Extract this to a util function for potential reuse
  const splitByWidth = (str: string, maxWidth: number) => {
    const result: string[] = [];

    let index = 0;
    let length = bisectWidth(str, maxWidth);
    while(length < str.slice(index).length) {
      result.push(str.slice(index, index + length));
      index += length;
      length = bisectWidth(str.slice(index), maxWidth);
    }

    result.push(str.slice(index));

    return result;
  };

  const x = centered ? width / 2 : 0;
  const drawLines = (str: string) => {
    let y = 0;

    const lines = str.split("\n");

    lines.forEach(line => {
      if(line.trim().length === 0) {
        y += actualSpacing;
        return;
      }

      const content = wordWrap ? splitByWidth(line, canvas.width) : [line];
      content.forEach(subline => {
        context.fillText(subline, x, y);
        y += actualSpacing;
      })
    });
  };

  const set = (...values: any[]) => {
    context.clearRect(0, 0, canvas.width, canvas.height);

    const str = values.map(toLog).join(" ");
    drawLines(str);

    if(border) {
      context.strokeStyle = border;
      context.strokeRect(0, 0, canvas.width, canvas.height);
    }

    texture.needsUpdate = true;
  };

  // We put it in a container so we can scale it evenly
  const container = new Object3D();
  container.add(mesh);

  return { mesh: container, set };
};
