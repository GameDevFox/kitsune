import { times } from "lodash";
import {
  BufferGeometry, DirectionalLight, Group, HemisphereLight, Line, LineBasicMaterial,
  Mesh, MeshStandardMaterial, Object3D, Object3DEventMap, Scene, SphereGeometry, Vector3,
} from "three";

import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

import { ConfigValue } from "../builder";

import { hotModule } from "../hot";

import { App } from "./app";
import { Aki } from "./objects/aki";
import { console, hud, liveConsole } from "./objects/console";
import { Crate } from "./objects/crate";
import { Plane } from "./objects/plane";
import { Wireframe } from "./objects/wireframe";
import { SudsTheCat } from "./objects/suds-the-cat";
import { circlePath, evenlySpaced, rotate, scale, spread } from "./utils/spread";

export type SceneObjects = Awaited<ReturnType<typeof buildSceneObjects>>;

export const buildSceneObjects = async ({ controls, state }: App) => {
  const { controllers, controllerGrips, hands } = controls;

  const scene = new Scene()

  const addToScene = (object: Object3D) => {
    scene.add(object);
  };

  controllers.forEach(addToScene);
  controllerGrips.forEach(addToScene);
  hands.forEach(addToScene);

  const aki = Aki();
  aki.position.z = -1;
  scene.add(aki);

  const headsetRig = new Object3D();
    hud.object.position.set(0, 0, -0.25);
    hud.object.visible = false;
    headsetRig.add(hud.object);
    hud.set("This is the console");
  scene.add(headsetRig);

  const rightControllerRig = new Object3D();
    const lineGeom = new BufferGeometry().setFromPoints([
      new Vector3(0, 0, 0),
      new Vector3(0, 0, -1)
    ]);
    const lineMat = new LineBasicMaterial({ color: 0xffffff, linewidth: 5 });
    const line = new Line(lineGeom, lineMat);
    line.visible = false;
    rightControllerRig.add(line);
  scene.add(rightControllerRig);

  const leftControllerGripRig = new Object3D();
    console.object.children[0].position.set(0, 0.11, 0);
    console.object.rotation.set(-Math.PI * (5 / 8), 0, -0.1);
    leftControllerGripRig.add(console.object);
  scene.add(leftControllerGripRig);

  const rightControllerGripRig = new Object3D();
    liveConsole.object.children[0].position.set(0, 0.11, 0);
    liveConsole.object.rotation.set(-Math.PI * (5 / 8), 0, 0.1);
    rightControllerGripRig.add(liveConsole.object);
  scene.add(rightControllerGripRig);

  const floor = Plane();
  floor.rotation.set(-(Math.PI / 2), 0, 0);
  floor.visible = false;
  scene.add(floor);

  const sphereGeom = new SphereGeometry();
  const sphereMat = new MeshStandardMaterial({ color: 0xffffff });
  const sphere = new Mesh(sphereGeom, sphereMat);
  sphere.scale.setScalar(0.3);
  sphere.position.set(-1, 1, -1);
  scene.add(sphere);

  scene.add(SudsTheCat());

  const loader = new GLTFLoader();
  const cube = await new Promise<Group<Object3DEventMap>>((resolve) => {
    loader.load("./models/dice.glb", object => {
      const cube = object.scene;
      cube.position.x = 1;
      cube.position.y = 1;
      cube.scale.set(0.2, 0.2, 0.2);

      resolve(cube);
    });
  });
  scene.add(cube);

  const raiden = await new Promise<Group<Object3DEventMap>>((resolve) => {
    loader.load("./models/raiden.glb", object => {
      const scene = object.scene;
      scene.position.x = -1.25;
      scene.position.z = 0;
      scene.rotation.y = Math.PI / 3;
      // scene.scale.set(0.2, 0.2, 0.2);

      resolve(scene);
    });
  });
  scene.add(raiden);

  // Lights
  const hemisphereLight = new HemisphereLight(0xffffff, 0x8d8d8d, 0.5);
  hemisphereLight.position.set(0, 20, 0);
  scene.add(hemisphereLight);

  [
    new Vector3(-1, 1, 0),
    new Vector3(3, 1, 2),
  ].forEach(pos => {
    const light = new DirectionalLight(0xffffff, 1);
    light.position.copy(pos);
    scene.add(light);
  });

  const wireframe = Wireframe(
    new Vector3(0, 0, 0),
    new Vector3(1, 1, 1),
  );
  scene.add(wireframe);

  const boxGroup = new Group();
  boxGroup.position.y = 1;
  boxGroup.position.z = -0.5;
    const crates = times(7, () => {
      const crate = Crate();
      crate.rotation.z = -Math.PI / 4;
      return crate;
    });
    crates.forEach(crate => boxGroup.add(crate));
  scene.add(boxGroup);

  state.target = crates[0];

  spread(
    crates,
    evenlySpaced(false),
    circlePath(0.3, Math.PI / 2),
    // linearPath(new Vector3(-1, 0, 0)),
    rotate(),
    scale(0.1),
  );

  return {
    scene,
    aki, boxGroup, cube, floor, line, sphere, wireframe,
    headsetRig,
    rightControllerRig,
    leftControllerGripRig, rightControllerGripRig,
  };
};

export const sceneObjects: ConfigValue<App> = [
  ["controls", "state"],
  buildSceneObjects,
];

hotModule("sceneObjects", buildSceneObjects);
if(module.hot) module.hot.accept();
