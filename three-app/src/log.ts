import axios from "axios";
import _ from "lodash";

import { api as apiConfig } from "./config";
import { consoleLog } from "./webgl/objects/console";
import { Matrix4 } from "three";

const { baseURL } = apiConfig;

const api = axios.create({ baseURL });

const queue: string[] = [];

setInterval(async () => {
  while(queue.length) {
    const message = queue.shift();

    if(message !== undefined)
      await api.post("/log", message.slice(0, 10000), { headers: { "Content-Type": "text/plain" } });
  }
}, 0);

// Unified logger
export const log = (...values: any[]) => {
  consoleLog(...values);
  netLog(...values);
};

export const netLog = (...values: any[]) => {
  const message = values.map(toLog).join(" ");
  queue.push(message);
};

export const toLog = (value: any) => {
  if(typeof value === "string")
    return value;

  if(typeof value === "function") {
    const fnSignature = value.toString()
      .split("\n")[0] // Take only the first line
      .slice(0, -2); // Remove " {" at the end of string
    return `function ${fnSignature}`;
  }

  return JSON.stringify(value, null, 2);
};

export const matrixToLines = (matrix: Matrix4, fixed: number = 3) => {
  const { elements } = matrix;

  const e = elements.map(value => value.toFixed(fixed).padStart(fixed+3));

  return [
    `[${e[0]}, ${e[4]}, ${e[8]}, ${e[12]}]`,
    `[${e[1]}, ${e[5]}, ${e[9]}, ${e[13]}]`,
    `[${e[2]}, ${e[6]}, ${e[10]}, ${e[14]}]`,
    `[${e[3]}, ${e[7]}, ${e[11]}, ${e[15]}]`,
  ];
};

export const ThrottledLog = (wait: number) => _.throttle(log, wait);
