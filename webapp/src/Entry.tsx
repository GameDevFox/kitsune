import { useEffect, useState } from "react";
import update from "immutability-helper";
import { Button, HStack, VStack } from "@chakra-ui/react";

const codes: Record<string, string> = {
  "Digit0": "0",
  "Digit1": "1",
  "Digit2": "2",
  "Digit3": "3",
  "Digit4": "4",
  "Digit5": "5",
  "Digit6": "6",
  "Digit7": "7",
  "Digit8": "8",
  "Digit9": "9",

  "KeyA": "A",
  "KeyB": "B",
  "KeyC": "C",
  "KeyD": "D",
  "KeyE": "E",
  "KeyF": "F",
  "KeyG": "G",
  "KeyH": "H",
  "KeyI": "I",
  "KeyJ": "J",
  "KeyK": "K",
  "KeyL": "L",
  "KeyM": "M",
  "KeyN": "N",
  "KeyO": "O",
  "KeyP": "P",
  "KeyQ": "Q",
  "KeyR": "R",
  "KeyS": "S",
  "KeyT": "T",
  "KeyU": "U",
  "KeyV": "V",
  "KeyW": "W",
  "KeyX": "X",
  "KeyY": "Y",
  "KeyZ": "Z",
};

export const Entry = () => {
  const [value, setValue] = useState([[""]]); // Lines, Words, Characters
  const [selection, setSelection] = useState([0, 0]); // Lines, Words, Characters

  const getSelection = () => {
    const realLineIndex = Math.min(selection[0], value.length - 1);
    const realWordIndex = Math.min(selection[1], value[realLineIndex].length - 1);

    return [realLineIndex, realWordIndex];
  };

  const getWord = (lineIndex: number, wordIndex: number) => {
    const result = value[lineIndex][wordIndex];

    if(result == null)
      console.warn(`Warning: Bad selection - ${selection}`);

    return result;
  }

  const setWord = (word: string, lineIndex: number, wordIndex: number) => {
    return update(value, {
      [lineIndex]: {
        [wordIndex]: {
          $set: word,
        }
      }
    });
  };

  const removeWord = (lineIndex: number, wordIndex: number) => {
    // Also remove line if this line is empty
    if(value[lineIndex].length <= 1)
      return update(value, {
        $splice: [[lineIndex, 1]],
      });

    return update(value, {
      [lineIndex]: {
        $splice: [[wordIndex, 1]],
      }
    });
  };

  const getSelectedWord = () => {
    const sel = getSelection();
    return getWord(sel[0], sel[1]);
  };
  const setSelectedWord = (word: string) => {
    const sel = getSelection();
    return setWord(word, sel[0], sel[1]);
  };
  const removeSelectedWord = () => {
    const sel = getSelection();
    return removeWord(sel[0], sel[1]);
  }

  const updateSelectedWord = (fn: (word: string) => string) => {
    const word = getSelectedWord()
    const newWord = fn(word);
    return setSelectedWord(newWord);
  };

  const operations: Record<string, any> = {
    "Escape": () => { // 0x01
      setSelection([0, 0]);
      return [[""]];
    },
    "Backspace": (value: string[][]) => { // 0x0e
      const word = getSelectedWord();
      if(word === "")
        return removeSelectedWord();
      else
        return setSelectedWord(word.slice(0, -1))
    },
    "Enter": (value: string[][]) => { // 0x1c
      const sel = getSelection();
      const newSelection = [sel[0] + 1, sel[1]];
      setSelection(newSelection)

      return update(value, { $push: [[""]] })
    },
    "Space": (value: string[][]) => { // 0x39
      const sel = getSelection();
      const newSelection = [sel[0], sel[1] + 1];
      setSelection(newSelection)

      return update(value, { [sel[0]]: { $splice: [[sel[1] + 1, 0, ""]] } });
    },
    "Delete": (value: string[][]) => { // 0x6f
      return removeSelectedWord();
    },

    "ArrowLeft": (value: string[][]) => { // 0xe04b
      const sel = getSelection();
      const newSelection = [sel[0], Math.max(0, sel[1] - 1)];
      setSelection(newSelection);

      return value;
    },

    "ArrowRight": (value: string[][]) => { // 0xe04b
      const sel = getSelection();
      const newSelection = [sel[0], Math.min(value[sel[0]].length - 1, sel[1] + 1)];
      setSelection(newSelection);

      return value;
    },

    "ArrowUp": (value: string[][]) => { // 0xe04b
      const sel = getSelection();
      const newSelection = [Math.max(0, sel[0] - 1), 0];
      setSelection(newSelection);

      return value;
    },

    "ArrowDown": (value: string[][]) => { // 0xe04b
      const sel = getSelection();
      const newSelection = [Math.min(value.length - 1, sel[0] + 1), 0];
      setSelection(newSelection);

      return value;
    },
  };

  const codeHandler = (code: string) => {
    const char = codes[code];
    if(char) {
      const newValue = updateSelectedWord(word => `${word}${char}`)
      setValue(newValue);
      return;
    }

    const op = operations[code];
    if(op) {
      const newValue = op(value);
      setValue(newValue);
      return;
    }

    console.log(`Ignoring keyDown event: ${code}`);
  };

  useEffect(() => {
    const listeners: Record<string, any> = {};

    ["keydown", "keyup"].forEach(name => {
      const listener = (e: KeyboardEvent) => {
        if(e.repeat)
          return;

        if(name === "keydown")
          codeHandler(e.code)
      };

      listeners[name] = listener;

      // console.log(`Adding event listener:`, name);
      window.addEventListener(name, listener as any);
    });

    return () => {
      Object.entries(listeners).forEach(([name, listener]) => {
        // console.log(`Removing event listener:`, name);
        window.removeEventListener(name, listener);
      });
    };
  }, [codeHandler]);

  const result = value.map((line, lineIndex) => {
    const words = line.map((word, wordIndex) => {
      const sel = getSelection();
      const selected = sel[0] === lineIndex && sel[1] === wordIndex;
      let colorPalette = word.trim() === "" ? "gray" : "blue";
      if(selected)
        colorPalette = "yellow";

      return <Button key={wordIndex} { ...{ colorPalette } }>{word}</Button>
    });

    return <HStack key={lineIndex}>{words}</HStack>
  });
  return <VStack>{result}</VStack>;
};
